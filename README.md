
## Feliz 5 meses, bb. Te amo mil milhões <3



![enter image description here](https://bitbucket.org/fiap_da_depressao/5/downloads/bb_5.jpeg)


Amo-te tanto, meu amor… não cante  

O humano coração com mais verdade…  
Amo-te como amigo e como amante  
Numa sempre diversa realidade  

Amo-te afim, de um calmo amor prestante,  
E te amo além, presente na saudade.  
Amo-te, enfim, com grande liberdade  
Dentro da eternidade e a cada instante.  

Amo-te como um bicho, simplesmente,  
De um amor sem mistério e sem virtude  
Com um desejo maciço e permanente.  

E de te amar assim muito e amiúde,  
É que um dia em teu corpo de repente  
Hei de morrer de amar mais do que pude.  

**Soneto do Amor Total**  
**Vinicius de Moraes**